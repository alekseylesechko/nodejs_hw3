import {verifyToken, validateAuth, validateLogin} from '../helpers/index.js';
import {User, Credentials} from '../models/index.js';

export async function checkToken(req, res, next) {
  const token = req.headers['authorization']?.split(' ')[1];
  if (!token) {
    return res.status(400).json({message: 'Access denied'});
  }
  const varify = verifyToken(token);
  if (!varify) {
    return res.status(400).json({message: 'Wrong token'});
  }
  const credentials = await Credentials.findOne({_id: varify._id});
  if (!credentials) {
    return res.status(400).json({message: 'User not exists'});
  }
  req.user = await User.findOne({email: credentials.email});
  req.user.role = credentials.role;
  req.credentialsId = credentials._id;
  next();
}

export async function checkAuthParams(req, res, next) {
  const validate = validateAuth(req.body);

  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}

export async function checkLoginParams(req, res, next) {
  const validate = validateLogin(req.body);

  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}

export async function checkForgotPasswordParams(req, res, next) {
  const validate = validateForgotPasswordParams(req.body);

  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}


export async function checkUserExists(req, res, next) {
  const userExists = await User.findOne({email: req.body.email});
  if (userExists) {
    return res.status(400).json({message: 'User already exists'});
  }
  next();
}
