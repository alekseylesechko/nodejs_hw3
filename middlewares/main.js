import fs from 'fs';

const writeStream = fs.createWriteStream('log.txt');

export function addInfo(req, res, next) {
  const info = {
    date: new Date().toLocaleString(),
    method: req.method,
    url: req.url,
    data: req.body,
  };
  writeStream.write(JSON.stringify(info) + '\n');
  next();
}

export function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next).catch(next);
  };
}
