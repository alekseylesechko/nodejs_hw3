import {
  validateChangingPassword,
  compareHashPassword,
  validateTruckAddParam,
  validateLoadAddParam,
} from '../helpers/index.js';
import {Credentials} from '../models/index.js';

export async function checkChangePasswordParam(req, res, next) {
  const validate = validateChangingPassword(req.body);
  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}

export async function checkTruckAddParam(req, res, next) {
  const validate = validateTruckAddParam(req.body);
  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}

export async function checkIsDriver(req, res, next) {
  const user = req.user;
  if (user.role != 'DRIVER') {
    return res.status(400).json({message: 'The user is not a driver'});
  }
  next();
}


export async function checkIsShipper(req, res, next) {
  const user = req.user;
  if (user.role != 'SHIPPER') {
    return res.status(400).json({message: 'The user is not a snipper'});
  }
  next();
}


export async function checkOldPassword(req, res, next) {
  try {
    const userCredentials = await Credentials.findOne({
      email: req.user.email,
    });
    const isPasswordValid = await compareHashPassword(
        req.body.oldPassword,
        userCredentials.password,
    );
    if (!isPasswordValid) {
      return res.status(400).json({message: 'Old password wrong'});
    }
    next();
  } catch (err) {
    {
      return res.status(500).json({message: err.message || 'Server error'});
    }
  }
}

export async function checkLoadAddParam(req, res, next) {
  const validate = validateLoadAddParam(req.body);
  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}
