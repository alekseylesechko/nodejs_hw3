import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const truck = new Schema(
    {
      name: {
        type: String,
        default: 'Truck',
      },
      createdBy: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
      assignedTo: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null,
      },
      status: {
        type: String,
        enum: ['IS', 'OL'],
        default: 'IS',
      },
      type: {
        type: String,
        required: true,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
      },
      dimensions: {
        width: {
          type: Number,
          required: true,
        },
        length: {
          type: Number,
          required: true,
        },
        height: {
          type: Number,
          required: true,
        },
      },
      payload: {
        type: Number,
        required: true,
        min: 0,
        max: 5000,
      },
    },
    {timestamps: true},
);

export const Truck = model('Truck', truck);
