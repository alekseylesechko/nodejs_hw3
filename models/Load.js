import mongoose from 'mongoose';
import {STATE, STATUS} from '../enum/index.js';
const {Schema, model, Types} = mongoose;

const load = new Schema(
    {
      createdBy: {type: Types.ObjectId, required: true, ref: 'User'},
      name: {type: String, required: true},
      logs: [
        {
          message: {type: String, required: true},
          time: {type: Date, default: Date.now},
        },
      ],
      assignedTo: {
        type: Types.ObjectId,
        ref: 'User',
        default: null,
      },
      status: {
        type: String,
        enum: STATUS,
        default: STATUS.A,
      },
      state: {
        type: String,
        enum: STATE,
        default: STATE.A,
      },
      dimensions: {
        width: {type: Number, required: true, min: 0, max: 1000},
        length: {type: Number, required: true, min: 0, max: 1000},
        height: {type: Number, required: true, min: 0, max: 1000},
      },
      payload: {type: Number, required: true, min: 0, max: 5000},
      pickupAddress: {type: String, required: true},
      deliveryAddress: {type: String, required: true},
    },
    {timestamps: true},
);

export const Load = model('Load', load);
