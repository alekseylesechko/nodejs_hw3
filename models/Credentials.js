import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const credentials = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    default: 'SHIPPER',
  },
});

export const Credentials = model('Credentials', credentials);
