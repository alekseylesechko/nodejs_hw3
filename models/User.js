import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const user = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  truckId: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Truck',
    },
  ],
});

user.methods.addTruck = function(truckId) {
  const trucks = [...this.truckId];
  trucks.push(truckId);
  this.truckId = trucks;
  return this.save();
};


export const User = model('User', user);
