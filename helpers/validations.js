import Joi from 'joi';

const changePasswordSchema = Joi.object({
  oldPassword: Joi.string().min(3).required(),
  newPassword: Joi.string().min(3).required(),
});

const truckAddParam = Joi.object({
  type: Joi.string()
      .required()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
});

const authSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(3).required(),
  role: Joi.string().min(3).required().valid('SHIPPER', 'DRIVER'),
});

const loginSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(3).required(),
});

const forgotPasswordSchema = Joi.object({
  email: Joi.string().email().required(),
});

const loadAddParam = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: {
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  },
});

export function validateChangingPassword(body) {
  return changePasswordSchema.validate(body);
}

export function validateTruckAddParam(body) {
  return truckAddParam.validate(body);
}

export function validateAuth(body) {
  return authSchema.validate(body);
}

export function validateLogin(body) {
  return loginSchema.validate(body);
}

export function validateForgotPasswordParams(body) {
  return forgotPasswordSchema.validate(body);
}

export function validateLoadAddParam(body) {
  return loadAddParam.validate(body);
}
