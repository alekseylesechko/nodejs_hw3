export function getTrucksForResponse(trucks) {
  return trucks.map((el) => {
    return {
      _id: el._id,
      assigned_to: el.assignedTo,
      created_by: el.createdBy,
      status: el.status,
      type: el.type,
      created_date: el.createdAt,
    };
  });
}

export function getShippingInfoResponse(load, truck) {
  return {
    load: {
      _id: load._id,
      created_by: load.createdBy,
      assigned_to: load.assignedTo,
      status: load.status,
      state: load.state,
      name: load.name,
      payload: load.payload,
      pickup_address: load.pickupAddress,
      delivery_address: load.deliveryAddress,
      dimensions: load.dimensions,
      logs: load.logs,
      created_date: load.createdAt,
    },
    truck: {
      _id: truck._id,
      created_by: truck.createdBy,
      assigned_to: truck.assignedTo,
      type: truck.type,
      status: truck.status,
      created_date: truck.createdAt,
    },
  };
}

export function getLoadsForResponse(trucks) {
  return trucks.map((el) => {
    return {
      _id: el._id,
      created_by: el.createdBy,
      assigned_to: el.assignedTo,
      status: el.status,
      state: el.state,
      name: el.name,
      payload: el.payload,
      pickup_address: el.pickupAddress,
      deliveryAddress: 'Sr. Rodrigo Domínguez Av. Bellavista N° 185',
      dimensions: el.dimensions,
      logs: getLogs(el.logs),
      created_date: el.createdAt,
    };
  });
}

export function getLoadsForCreate(load, userId) {
  const logs = [
    {
      message: 'Load created',
      time: Date.now(),
    },
  ];
  return {
    createdBy: userId,
    status: 'NEW',
    name: load.name,
    payload: load.payload,
    pickupAddress: load.pickup_address,
    deliveryAddress: load.delivery_address,
    dimensions: load.dimensions,
    logs: logs,
  };
}

function getLogs(logs) {
  return logs.map((el) => {
    return {
      message: el.message,
      time: el.time,
    };
  });
}
