import {STATE} from '../enum/index.js';

export function getNextState(load) {
  const entries = Object.entries(STATE);
  const idx = entries.findIndex((el) => el[1] === load.state);

  if (idx < entries.length - 1) {
    return STATE[entries[idx + 1][0]];
  }
  return load.state;
}
