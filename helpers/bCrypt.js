import bCrypt from 'bcryptjs';

async function getSolt() {
  return await bCrypt.genSalt(10);
}

export async function createHashPassword(password) {
  return await bCrypt.hash(password.toString(), await getSolt());
}

export async function compareHashPassword(userPassword, BDPassword) {
  return await bCrypt.compare(userPassword, BDPassword);
}
