import express from 'express';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import cors from 'cors';
import {addInfo} from './middlewares/index.js';
import {
  usersRouter,
  trucksRouter,
  authRouter,
  loadsRouter,
} from './routes/index.js';

dotenv.config();
// eslint-disable-next-line
const app = new express();
const PORT = process.env.PORT || 8080;
start();

// Middleware
app.use(cors());
app.use(express.json());
app.use(addInfo);

// Route middlewares
app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

// eslint-disable-next-line
app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

async function start() {
  try {
    await mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
  } catch (error) {
    logging('Error', error.message);
    process.exit(1);
  }
}
