import express from 'express';
import {getUser, deleteUser, updatePassword} from '../controllers/users.js';
import {
  checkToken,
  asyncWrapper,
  checkOldPassword,
  checkChangePasswordParam,
} from '../middlewares/index.js';

// eslint-disable-next-line
export const router = express.Router();
router.use(asyncWrapper(checkToken));

// GET api/users/me
router.get('/', asyncWrapper(getUser));

// PATCH api/users/me/password
router.patch(
    '/password',
    checkChangePasswordParam,
    checkOldPassword,
    asyncWrapper(updatePassword),
);

// DELETE api/users/me
router.delete('/', asyncWrapper(deleteUser));
