import express from 'express';
import {
  getLoads,
  getActiveLoad,
  addLoad,
  changeActiveStateLoad,
  getUserLoadById,
  updateUserLoadById,
  postUserLoadById,
  deleteUserLoadById,
  getUserLoadShippingInfo,
} from '../controllers/loads.js';
import {
  checkToken,
  asyncWrapper,
  checkLoadAddParam,
  checkIsShipper,
  checkIsDriver,
} from '../middlewares/index.js';

// eslint-disable-next-line
export const router = express.Router();
router.use(asyncWrapper(checkToken));

// GET api/loads
router.get('/', asyncWrapper(getLoads));

// POST api/loads
router.post('/', checkIsShipper, checkLoadAddParam, asyncWrapper(addLoad));

// GET api/loads/active
router.get('/active', checkIsDriver, asyncWrapper(getActiveLoad));

// PATCH api/loads/active/state
router.patch(
    '/active/state',
    checkIsDriver,
    asyncWrapper(changeActiveStateLoad),
);

// GET api/loads/:id
router.get('/:id', asyncWrapper(getUserLoadById));

// PUT /api/loads/:id
router.put(
    '/:id',
    checkIsShipper,
    checkLoadAddParam,
    asyncWrapper(updateUserLoadById),
);

// DELETE api/loads/:id
router.delete('/:id', checkIsShipper, asyncWrapper(deleteUserLoadById));

// POST api/loads/:id/post
router.post('/:id/post', checkIsShipper, asyncWrapper(postUserLoadById));

// GET /api/loads/{id}/shipping_info
router.get(
    '/:id/shipping_info',
    checkIsShipper,
    asyncWrapper(getUserLoadShippingInfo),
);
