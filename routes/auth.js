import express from 'express';
import {register, login, forgotPassword} from '../controllers/auth.js';
import {
  checkAuthParams,
  checkUserExists,
  checkLoginParams,
  checkForgotPasswordParams,
  asyncWrapper,
} from '../middlewares/index.js';

// eslint-disable-next-line
export const router = express.Router();

// POST api/auth/register
router.post(
    '/register',
    checkAuthParams,
    checkUserExists,
    asyncWrapper(register),
);

// POST api/auth/login
router.post('/login', checkLoginParams, asyncWrapper(login));

// POST /api/auth/forgot_password
router.post(
    '/forgot_password',
    checkForgotPasswordParams,
    asyncWrapper(forgotPassword),
);
