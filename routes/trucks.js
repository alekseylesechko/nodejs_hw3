import express from 'express';
import {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
} from '../controllers/trucks.js';
import {
  checkToken,
  asyncWrapper,
  checkTruckAddParam,
  checkIsDriver,
} from '../middlewares/index.js';

// eslint-disable-next-line
export const router = express.Router();
router.use(asyncWrapper(checkToken));
router.use(checkIsDriver);

// GET api/trucks
router.get('/', asyncWrapper(getTrucks));

// POST api/trucks
router.post('/', checkTruckAddParam, asyncWrapper(addTruck));

// GET api/trucks/:id
router.get('/:id', asyncWrapper(getTruckById));

// PUT api/trucks/:id
router.put('/:id', checkTruckAddParam, asyncWrapper(updateTruck));

// DELETE api/trucks/:id
router.delete('/:id', asyncWrapper(deleteTruck));

// POST api/truck/{id}/assign
router.post('/:id/assign', asyncWrapper(assignTruck));
