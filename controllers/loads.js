import {Load, Truck} from '../models/index.js';
import {
  getLoadsForResponse,
  getLoadsForCreate,
  getShippingInfoResponse,
  getNextState,
} from '../helpers/index.js';
import {STATUS} from '../enum/index.js';

export async function getLoads(req, res) {
  const pageNo = parseInt(req.query.offset) || 1;
  const size = parseInt(req.query.limit) || 0;
  const status = req.query.status ? {status: req.query.status} : {};
  const options = {
    limit: size,
    skip: size * (pageNo - 1),
  };
  const user = req.user;

  if (user.role === 'SHIPPER') {
    const loads = await Load.find(
        {createdBy: user._id, ...status},
        {},
        options,
    );
    if (loads) {
      const response = getLoadsForResponse(loads);
      return res.status(200).json({loads: response});
    }
    return res.status(400).json({message: 'Load not found'});
  }

  if (user.role === 'DRIVER') {
    const status = req.query.status ? {status: req.query.status} : {};
    const loads = await Load.find(
        {assignedTo: user._id, ...status},
        {},
        options,
    );
    if (loads) {
      const response = getLoadsForResponse(loads);
      return res.status(200).json({loads: response});
    }
    return res.status(400).json({message: 'Load not found'});
  }

  return res.status(400).json({message: 'User not found'});
}

export async function addLoad(req, res) {
  const newLoad = getLoadsForCreate(req.body, req.user._id);
  const load = new Load(newLoad);
  await load.save();
  return res.status(200).json({message: 'Load created successfully'});
}

export async function getActiveLoad(req, res) {
  const user = req.user;
  const load = await Load.findOne({assignedTo: user._id});
  if (!load) {
    return res.status(400).json({message: 'No active load'});
  }
  const response = getLoadsForResponse([load]);
  return res.status(200).json({load: response[0]});
}

export async function changeActiveStateLoad(req, res) {
  const user = req.user;
  const load = await Load.findOne({assignedTo: user._id});
  if (!load) {
    return res.status(400).json({message: 'No active load'});
  }

  const nextState = getNextState(load);
  load.state = nextState;
  await load.save();
  const log = `Load state changed to '${nextState}'`;

  return res.status(200).json({message: log});
}

export async function getUserLoadById(req, res) {
  const user = req.user;
  const id = req.params.id;
  const load = await Load.findOne({createdBy: user._id, _id: id});
  if (!load) {
    return res.status(400).json({message: 'No active load'});
  }
  const response = getLoadsForResponse([load]);
  res.status(200).json({load: response[0]});
}

export async function updateUserLoadById(req, res) {
  const id = req.params.id;
  const load = await Load.findById(id);
  if (!load) {
    return res.status(400).json({message: 'No active load'});
  }
  if (load.status !== STATUS.A) {
    return res.status(400).json({message: 'You can not update active load'});
  }
  await Load.findByIdAndUpdate(id, req.body);
  res.status(200).json({message: 'Load details changed successfully'});
}

export async function deleteUserLoadById(req, res) {
  const id = req.params.id;
  const load = await Load.findById(id);
  if (!load) {
    return res.status(400).json({message: 'No active load'});
  }
  if (load.status !== STATUS.A) {
    return res.status(400).json({message: 'You can not delete active load'});
  }
  await Load.findByIdAndDelete(id);
  res.status(200).json({message: 'Load deleted successfully'});
}

export async function postUserLoadById(req, res) {
  const id = req.params.id;
  const load = await Load.findById(id);

  if (load) {
    const {
      dimensions: {height, width, length},
      payload,
    } = load;

    const matchedTruck = await Truck.findOne()
        .where('status')
        .equals('IS')
        .where('payload')
        .gt(payload)
        .where('dimensions.height')
        .gt(height)
        .where('dimensions.width')
        .gt(width)
        .where('dimensions.length')
        .gt(length);

    if (matchedTruck) {
      matchedTruck.status = 'OL';
      load.status = 'ASSIGNED';
      load.assignedTo = matchedTruck.createdBy;
      load.state = 'En route to Pick Up';
      load.truck = matchedTruck._id;
      load.logs = [
        ...load.logs,
        {
          message: `Load assigned to driver with id ${matchedTruck.createdBy}`,
        },
      ];

      await matchedTruck.save();
      await load.save();
      res
          .status(200)
          .json({message: 'Load posted successfully', driver_found: true});
    } else {
      load.logs = [...load.logs, {message: `Could't not find driver`}];
      await load.save();
      return res.status(400).json({message: 'No Available Driver Found'});
    }
  }
}

export async function getUserLoadShippingInfo(req, res) {
  const id = req.params.id;
  const load = await Load.findById(id);
  const truck = await Truck.findOne({assignedTo: load.assignedTo});
  if (!load) {
    return res.status(400).json({message: 'No active load'});
  }
  const response = getShippingInfoResponse(load, truck);
  res.status(200).json({load: response});
}
