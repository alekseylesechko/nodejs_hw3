import {Truck} from '../models/index.js';
import {getTrucksForResponse} from '../helpers/index.js';
import {TRUCKS} from '../enum/index.js';

export async function getTrucks(req, res) {
  const trucks = await Truck.find({createdBy: req.user._id});
  if (trucks) {
    const response = getTrucksForResponse(trucks);
    return res.status(200).json({trucks: response});
  }
  return res.status(400).json({message: 'Trucks not found'});
}

export async function addTruck(req, res) {
  const type = req.body.type;
  const truck = new Truck({
    createdBy: req.user._id,
    type,
    ...TRUCKS[type],
  });
  await truck.save();
  await req.user.addTruck(truck._id);
  res.status(200).json({message: 'Truck created successfully'});
}

export async function getTruckById(req, res) {
  const id = req.params.id;
  const truck = await Truck.findById(id);
  if (!truck) {
    return res.status(400).json({message: 'Truck is not defined'});
  }
  const response = getTrucksForResponse([truck]);
  return res.status(200).json({
    truck: response[0],
  });
}

export async function updateTruck(req, res) {
  const id = req.params.id;
  const user = req.user;

  const truck = await Truck.findOne({_id: id, createdBy: user._id});
  if (!truck) {
    return res.status(400).json({message: 'Truck is not defined'});
  }
  if (truck.assignedTo) {
    return res
        .status(400)
        .json({message: 'Truck assigned, you can not change it'});
  }

  const newType = req.body.type;
  truck.type = newType;
  truck.dimensions = TRUCKS[newType].dimensions;
  truck.payload = TRUCKS[newType].payload;
  await truck.save();
  return res.status(200).json({message: 'Truck details changed successfully'});
}

export async function deleteTruck(req, res) {
  const user = req.user;
  const id = req.params.id;

  const truck = await Truck.findOne({_id: id, createdBy: user._id});
  if (!truck) {
    return res.status(400).json({message: 'Truck is not defined'});
  }
  if (truck.assignedTo) {
    return res
        .status(400)
        .json({message: 'Truck assigned, you can not delete it'});
  }
  await Truck.findByIdAndDelete(id);
  return res.status(200).json({message: 'Truck deleted successfully'});
}

export async function assignTruck(req, res) {
  const user = req.user;
  const id = req.params.id;
  const truck = await Truck.findOne({_id: id, createdBy: user._id});

  if (!truck) {
    return res.status(400).json({message: 'Truck is not defined'});
  }
  truck.assignedTo = user._id;
  truck.status = 'IS';
  await truck.save();
  return res.status(200).json({message: 'Truck assigned successfully'});
}
