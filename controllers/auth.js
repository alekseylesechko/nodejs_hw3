import {User, Credentials} from '../models/index.js';
import {
  createHashPassword,
  compareHashPassword,
  getToken,
} from '../helpers/index.js';

export async function register(req, res) {
  const password = await createHashPassword(req.body.password);

  const user = new User({
    email: req.body.email,
  });
  const credentials = new Credentials({
    email: req.body.email,
    role: req.body.role,
    password,
  });

  await user.save();
  await credentials.save();
  return res.status(200).json({message: 'Profile created successfully'});
}

export async function login(req, res) {
  const userCredentials = await Credentials.findOne({
    email: req.body.email,
  });
  if (!userCredentials) {
    return res.status(400).json({message: 'User not found'});
  }
  const isPasswordValid = await compareHashPassword(
      req.body.password,
      userCredentials.password,
  );
  if (!isPasswordValid) {
    return res.status(400).json({message: 'Password wrong'});
  }
  const token = getToken(userCredentials._id);

  return res.status(200).json({
    jwt_token: token,
  });
}

export async function forgotPassword(req, res) {
  const userCredentials = await Credentials.findOne({
    email: req.body.email,
  });
  if (!userCredentials) {
    return res.status(400).json({message: 'User not found'});
  }
  // /change password ?
  return res.status(200).json({
    message: 'New password sent to your email address',
  });
}
