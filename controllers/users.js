import {User, Credentials} from '../models/index.js';
import {createHashPassword} from '../helpers/index.js';

export async function getUser(req, res) {
  const user = await User.findOne({_id: req.user._id}, {__v: 0, truckId: 0});
  if (user) {
    return res.status(200).json({user});
  }
  return res.status(400).json({message: 'User not found'});
}

export async function deleteUser(req, res) {
  const user = await User.findOneAndDelete({_id: req.user._id});
  if (user) {
    await Credentials.findByIdAndDelete(req.credentialsId);
    return res.status(200).json({message: 'Profile deleted successfully'});
  }
  return res.status(400).json({message: 'User not found'});
}

export const updatePassword = async (req, res) => {
  const password = await createHashPassword(req.body.newPassword);
  await Credentials.findByIdAndUpdate(req.credentialsId, {
    password,
  });
  return res.status(200).json({message: 'Password changed successfully'});
};
