export {STATUS} from './status.js';
export {STATE} from './state.js';
export {TRUCKS_TYPES, TRUCKS} from './trucks.js';
