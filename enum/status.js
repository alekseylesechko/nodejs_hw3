const A = 'NEW';
const B = 'POSTED';
const C = 'ASSIGNED';
const D = 'SHIPPED';

export const STATUS = {A, B, C, D};
