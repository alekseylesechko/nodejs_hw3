const A = 'Ready to Pick Up';
const B = 'En route to Pick Up';
const C = 'Arrived to Pick Up';
const D = 'En route to Delivery';
const E = 'Arrived to Delivery';

export const STATE = {A, B, C, D, E};
